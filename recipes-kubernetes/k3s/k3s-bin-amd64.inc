require k3s-bin.inc
SRC_URI = "https://github.com/rancher/k3s/releases/download/v${K3S_VERSION}/k3s;unpack=0;name=k3s;downloadfilename=k3s-${PV} \
           https://github.com/rancher/k3s/releases/download/v${K3S_VERSION}/k3s-airgap-images-amd64.tar;unpack=0;name=k3s-airgap;downloadfilename=k3s-airgap-images-amd64-${PV}.tar"
SRC_URI[k3s.sha256sum] = "81ab4cc4c7c24b731c62c77c782eac9ccfe1e29e3d6b7e38ad7b4ea424f9713b"
SRC_URI[k3s-airgap.sha256sum] = "16c06d8571d777d807997d65131d554bf89b1dc391019d182b3f3f99d761a42b"
