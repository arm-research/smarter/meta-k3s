require k3s-bin.inc
SRC_URI = "https://github.com/rancher/k3s/releases/download/v${K3S_VERSION}/k3s-arm64;unpack=0;name=k3s-arm64;downloadfilename=k3s-${PV} \
           https://github.com/rancher/k3s/releases/download/v${K3S_VERSION}/k3s-airgap-images-arm64.tar;unpack=0;name=k3s-airgap;downloadfilename=k3s-airgap-images-arm64-${PV}.tar"
SRC_URI[k3s-arm64.sha256sum] = "94b6a378272236993248a73a96a5794ad69faf0d4d1fa1b9bdc9d04dd8538f81"
SRC_URI[k3s-airgap.sha256sum] ="67b02a534368c0b569a2e9556b56fe2da6564c186b949642659e3b4d12082665"






